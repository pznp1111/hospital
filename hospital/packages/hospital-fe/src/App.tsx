import React, { useState } from 'react';
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import './App.css';
import { PatientsRegister } from 'hospital-lib';
import {  Quarantine } from 'hospital-lib';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import { timingSafeEqual } from 'crypto';


interface ServiceResponse<T> {
  success: boolean;
  error?: string;
  data?: T
}


const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

interface Props {
  message: string;
}
interface State {
  runningKey:number;
  reports:any[];
  rows:any[];
  resultRows:any[];
  quarantineList:Quarantine[];
  drugList:any[];
}

export default class App extends React.Component<Props, State> {

  static defaultProps = { message: "hello" };
  static propTypes = {
    message: PropTypes.number
  };

  constructor(props: any) {
    super(props);
    this.state = {
      runningKey:1,
      reports:[],
      rows:[],
      resultRows:[],
      quarantineList:[],
      drugList:[]
    };
  }

  //var [reports, setReports] = useState<any[]>([])
  // var [runningKey, setRunningKey] = useState(1)


   createData(id:number,drugs:string, result:string, fever:string, healthy:string, diabetic:string, tuberculosis:string, dead:string) {
    return { id,drugs, result, fever, healthy, diabetic, tuberculosis, dead };
  }





  componentDidMount() {
    // let temprows = [
    //   this.createData(1,"1->1","1->1","1->1","1->1","1->1","1->1",""),
    //   this.createData(2,"1->1","1->1","1->1","1->1","1->1","1->1",""),
    //   this.createData(3,"1->1","1->1","1->1","1->1","1->1","1->1",""),
    //   this.createData(4,"1->1","1->1","1->1","1->1","1->1","1->1",""),
    //   this.createData(5,"1->1","1->1","1->1","1->1","1->1","1->1",""),
    //   this.createData(6,"1->1","1->1","1->1","1->1","1->1","1->1",""),
    //   this.createData(7,"1->1","1->1","1->1","1->1","1->1","1->1",""),
    //   this.createData(8,"1->1","1->1","1->1","1->1","1->1","1->1",""),
    // ];

    // this.setState({rows:temprows})
    console.log("rows",this.state.rows);
  }

   fetchSimulationData = () => {
    return Promise.all([
      restApi("/patients").then<ServiceResponse<PatientsRegister>>(response => {
        var { success, error, data = "" } = response;
        //console.log("/patients",response);
        if (success) {
          return {
            ...response,
            data: data
              .split(",")
              .reduce<PatientsRegister>((o, s) => {
                o[s] += 1;
                return o;
              }, emptyRegister())
          }
        } else {
          return { success, error }
        }
      }),
      restApi("/drugs").then<ServiceResponse<string[]>>(response => {
        var { success, error, data = "" } = response;
        if (success) {
          return {
            ...response,
            data: data.split(",")
          }
        }
        return { success, error }
      })
    ]).then(([patientsReponse, drugsReponse]) => {
      if (patientsReponse.error || drugsReponse.error) {
        throw [patientsReponse.error, drugsReponse.error]
      }

      return {
        drugs: drugsReponse.data,
        patientRegister: patientsReponse.data
      }
    }).catch(errors => alert(JSON.stringify(errors)))
  }

   simulate = () => {
    return this.fetchSimulationData()
      .then((data) => {
        // console.log("data",data);

        var { drugs, patientRegister } = data as any;

        return { drugs, patientRegister }
      })
      .then(({ drugs, patientRegister }) => {
        this.simulateAndReport(patientRegister, drugs);
      })
  }

  newSimulate = () =>{
    return this.fetchSimulationData()
    .then((data) => {
      console.log("data",data);

      var { drugs, patientRegister } = data as any;
      console.log("drugs",drugs);
      this.state.drugList.push(drugs)
      this.setState({drugList:this.state.drugList});
      return { drugs, patientRegister }
    })
    .then(({ drugs, patientRegister }) => {
      this.createSimulation(patientRegister, drugs);
    })
  }

  runSumulation = () =>{
    var nextRow = this.state.resultRows.length;
    console.log("this.state.quarantineList",this.state.quarantineList);
    console.log("this.state.drugList",this.state.drugList);
    console.log("nextRow",nextRow);
    if(this.state.quarantineList.length-1>=nextRow &&this.state.drugList.length-1 >=nextRow){
      var drugs = this.state.drugList[nextRow];
      console.log("test-1",drugs);


      console.log("test0",this.state.quarantineList[nextRow]);
      this.state.quarantineList[nextRow].setDrugs(drugs);
      this.state.quarantineList[nextRow].wait40Days()

      console.log("test1",this.state.quarantineList[nextRow]);
    } else{
      alert("insertion error");
      return;
    }

    this.simulateRow(
      { key: nextRow,
       drugs,
       result: this.state.quarantineList[nextRow].report(),
       //prior: { ...patientRegister }
      }
    );
  }

  makeNewRow = (report: any) =>{
    var reportsTmp = [report, ...this.state.reports];
    console.log("reportsTmp",reportsTmp);




    var tempRow = this.createData(this.state.runningKey,(reportsTmp[0]['drugs']).join(","),
    "",
    (reportsTmp[0]['result']['F']).toString(),
    (reportsTmp[0]['result']['H']).toString(),
    (reportsTmp[0]['result']['D']).toString(),
    (reportsTmp[0]['result']['T']).toString(),
    (reportsTmp[0]['result']['X']).toString()
    );
    console.log("tempRow",tempRow);

    this.state.rows.push(tempRow);
    this.setState({rows:this.state.rows});
    console.log("this.state.rows",this.state.rows);

  }

  simulateRow = (report: any) =>{
    var reportsTmp = [report, ...this.state.reports];
    console.log("reportsTmp1",reportsTmp)
    console.log("report",report)

     var modifingRowIndex = report['key'];
     var prevRowValue = this.state.rows[modifingRowIndex];
    console.log("prevRowValue",prevRowValue);
    var tempRow = this.createData(
    modifingRowIndex,(reportsTmp[0]['drugs']).join(","),
    "",
    (prevRowValue['fever']).toString() +"->" + (reportsTmp[0]['result']['F']).toString(),
    (prevRowValue['healthy']).toString() +"->" +(reportsTmp[0]['result']['H']).toString(),
    (prevRowValue['diabetic']).toString() +"->" +(reportsTmp[0]['result']['D']).toString(),
    (prevRowValue['tuberculosis']).toString() +"->" +(reportsTmp[0]['result']['T']).toString(),
    (prevRowValue['dead']).toString() +"->" +(reportsTmp[0]['result']['X']).toString()
    );
    // console.log("tempRow",tempRow);

    // this.state.rows.push(tempRow);
    //
    this.state.rows[modifingRowIndex] = tempRow;
    this.state.resultRows.push(tempRow);
    this.setState({resultRows:this.state.resultRows})
    this.setState({rows:this.state.rows})
    console.log("this.state.rows after",this.state.rows);

  }

  createSimulation = (patientRegister: PatientsRegister, drugs: string[]) => {
    var simulator = new Quarantine(patientRegister);
    simulator.setDrugs(drugs);
    this.setState({runningKey:this.state.runningKey+1})
    this.makeNewRow(
      { key: this.state.runningKey,
       drugs,
       result: simulator.report(),
       //prior: { ...patientRegister }
      }
    );

    this.state.quarantineList.push(simulator);
    this.setState({quarantineList:this.state.quarantineList})

  }

   simulateAndReport = (patientRegister: PatientsRegister, drugs: string[]) => {
    var simulator = new Quarantine(patientRegister);
    simulator.setDrugs(drugs);
    simulator.wait40Days();
    this.setState({runningKey:this.state.runningKey+1})
    //setRunningKey(runningKey + 1);
    this.addReport({ key: `#${this.state.runningKey}`, drugs, result: simulator.report(), prior: { ...patientRegister } });
  }

   addReport = (report: any) => {
    var reportsTmp = [report, ...this.state.reports];
    console.log("reportsTmp",reportsTmp)
    this.setState({reports:reportsTmp.filter(x => !!x)});
    console.log("this.state.reports",this.state.reports)
    //setReports(reportsTmp.filter(x => !!x))
  }
render(){
  return (
    <div className="App" style={{ width: "100%", height: "100% " }}>

      <Box sx={{ flexGrow: 1 }}>
      <Grid container >
        <Grid item xs={6}>
          <Item>  <button onClick={this.newSimulate}>New Simulation</button></Item>
        </Grid>
        <Grid item xs={6}>
          <Item><button onClick={this.runSumulation}>Run Simulation</button></Item>
        </Grid>
      </Grid>


      <Grid container >
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Id</TableCell>
                  <TableCell align="right">Administered Drugs</TableCell>
                  <TableCell align="right">Result</TableCell>
                  <TableCell align="right">Fever</TableCell>
                  <TableCell align="right">Healthy</TableCell>
                  <TableCell align="right">Diabetic</TableCell>
                  <TableCell align="right">Tuberculosis</TableCell>
                  <TableCell align="right">Dead</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.rows.map((row) => (
                  <TableRow
                    key={row.id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {row.id}
                    </TableCell>
                    <TableCell align="right">{row.drugs}</TableCell>
                    <TableCell align="right">{row.result}</TableCell>
                    <TableCell align="right">{row.fever}</TableCell>
                    <TableCell align="right">{row.healthy}</TableCell>
                    <TableCell align="right">{row.diabetic}</TableCell>
                    <TableCell align="right">{row.tuberculosis}</TableCell>
                    <TableCell align="right">{row.dead}</TableCell>
                  </TableRow>

                ))}
              </TableBody>
            </Table>
          </TableContainer>
      </Grid>


      </Box>
    </div>

  );
}
};

var emptyRegister = () => ({ F: 0, H: 0, D: 0, T: 0, X: 0 });

var restApi = (path: string) => fetch("http://localhost:7200" + path).then<ServiceResponse<string>>(resp => {
  if (resp.status === 200) {
    return resp.json().then(data => ({
      success: true,
      data
    }))
  } else {
    return {
      success: false,
      error: `Something happened. Backend sends us status: ${resp.status}`
    }
  }
})

var patientMap: { [key: string]: string } = {
  F: "Fever",
  H: "Healthy",
  D: "Diabetic",
  T: "Tuberculosis",
  X: "Dead"
}

var drugsMap: { [key: string]: string } = {
  "": "None",
  As: "Aspirin",
  An: "Antibiotic",
  I: "Insulin",
  P: "Paracetamol",
  "An,I": "Antibiotic + Insulin",
  "As,P": "Paracetamol + Aspirin"
}
