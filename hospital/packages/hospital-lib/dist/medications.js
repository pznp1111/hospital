"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.medicationShortFormStringtoObj = exports.medicineShortFormList = exports.medicineList = exports.Medicine = void 0;
var Medicine;
(function (Medicine) {
    Medicine["Aspirin"] = "As";
    Medicine["Antibiotic"] = "An";
    Medicine["Insulin"] = "I";
    Medicine["Paracetamol"] = "P";
})(Medicine = exports.Medicine || (exports.Medicine = {}));
exports.medicineList = [Medicine.Aspirin, Medicine.Antibiotic, Medicine.Insulin, Medicine.Paracetamol];
exports.medicineShortFormList = ['As', 'An', 'I', "P"];
function medicationShortFormStringtoObj(disease) {
    if (disease == 'As') {
        return Medicine.Aspirin;
    }
    if (disease == 'An') {
        return Medicine.Antibiotic;
    }
    if (disease == 'I') {
        return Medicine.Insulin;
    }
    if (disease == 'P') {
        return Medicine.Paracetamol;
    }
    return;
}
exports.medicationShortFormStringtoObj = medicationShortFormStringtoObj;
//# sourceMappingURL=medications.js.map