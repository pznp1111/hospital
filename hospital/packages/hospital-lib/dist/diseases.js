"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiseaseMapShortFormToFull = exports.DiseaseMapToFull = exports.diseaseShortFormList = exports.Disease = void 0;
var Disease;
(function (Disease) {
    Disease["Fever"] = "F";
    Disease["Healthy"] = "H";
    Disease["Diabetes"] = "D";
    Disease["Tuberculosis"] = "T";
    Disease["Dead"] = "X";
})(Disease = exports.Disease || (exports.Disease = {}));
exports.diseaseShortFormList = ["F", "H", "D", "T", "X"];
function DiseaseMapToFull(disease) {
    if (disease == Disease.Fever) {
        return 'Fever';
    }
    if (disease == Disease.Healthy) {
        return 'Healthy';
    }
    if (disease == Disease.Diabetes) {
        return 'Diabetes';
    }
    if (disease == Disease.Tuberculosis) {
        return 'Tuberculosis';
    }
    if (disease == Disease.Dead) {
        return 'Dead';
    }
    return;
}
exports.DiseaseMapToFull = DiseaseMapToFull;
function DiseaseMapShortFormToFull(disease) {
    if (disease == 'F') {
        return 'Fever';
    }
    if (disease == 'H') {
        return 'Healthy';
    }
    if (disease == 'D') {
        return 'Diabetes';
    }
    if (disease == 'T') {
        return 'Tuberculosis';
    }
    if (disease == 'X') {
        return 'Dead';
    }
    return;
}
exports.DiseaseMapShortFormToFull = DiseaseMapShortFormToFull;
// - F: Fever
// - H: Healthy
// - D: Diabetes
// - T: Tuberculosis
// - X: Dead
//# sourceMappingURL=diseases.js.map