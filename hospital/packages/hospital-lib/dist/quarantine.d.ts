import { PatientsRegister } from './patientsRegister';
import { Patient } from './patients';
export declare class Quarantine {
    private patients;
    patientList: Array<Patient>;
    private medicationHistory;
    /***
     * Args:PatientsRegister with the following format `{F:NP,H:NP,D:NP,T:NP,X:NP}`
     *  F, H, D, T, X are patients’ health status codes;
        - F: Fever
        - H: Healthy
        - D: Diabetes
        - T: Tuberculosis
        - X: Dead
     *  NP is a number of patients for a given state;
     *  E.g. `{F:0,H:2,D:0,T:0,X:1}` means there are two healthy patients and one that is dead.
     *
     * ** */
    constructor(patients: PatientsRegister);
    /***  define which drugs will be given to all patients.
     * The argument to this method is an array of drugs codes,
        - As: Aspirin
        - An: Antibiotic
        - I: Insulin
        - P: Paracetamol
     * e.g. [“As”,“I”] means patients will be treated with Aspirin and Insulin;
     *
     */
    setDrugs(drugs: Array<string>): void;
    /** will administer the drugs to the patients;
     *
     */
    wait40Days(): void;
    /*** should return an object describing the current states of the patients, following the same format as the constructor argument.
     *
     */
    report(): PatientsRegister;
    private makePatients;
    private setPatientNumberFromListByDisease;
    private drugEffect;
    private DiseaseMapFullToShortForm;
}
export declare class TemporaryQuarantine {
    private patients;
    constructor(patients: PatientsRegister);
    setDrugs(drugs: Array<string>): void;
    wait40Days(): void;
    report(): PatientsRegister;
}
