"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Patient = void 0;
const diseases_1 = require("./diseases");
//import {MedicineMap} from './patientsRegister';
class Patient {
    //private medicationHistory:Array<Medicine>;
    // private readonly diseaseMap ={
    //     "F": "Fever",
    //     "H": "Healthy",
    //     "D": "Diabetes",
    //     "T": "Tuberculosis",
    //     "X": "Dead"
    //   }
    constructor(disease, size) {
        this.disease = disease;
        this.size = size;
        // if (!this.diseaseMap.hasOwnProperty(disease)){
        //     throw new Error(Patient.NOT_IMPLEMENTED_MESSAGE);
        // } else{
        //     this.disease = disease;
        // }   
    }
    getSize() {
        return this.size;
    }
    increment(qty) {
        this.size += qty;
    }
    clear() {
        this.size = 0;
    }
    getDisease() {
        if (this.disease == 'Fever') {
            return diseases_1.Disease.Fever;
        }
        if (this.disease == 'Healthy') {
            return diseases_1.Disease.Healthy;
        }
        if (this.disease == 'Diabetes') {
            return diseases_1.Disease.Diabetes;
        }
        if (this.disease == 'Tuberculosis') {
            return diseases_1.Disease.Tuberculosis;
        }
        if (this.disease == 'Dead') {
            return diseases_1.Disease.Dead;
        }
        //return this.disease;
    }
    getDiseaseShortForm() {
        if (this.disease == 'Fever') {
            return 'F';
        }
        if (this.disease == 'Healthy') {
            return 'H';
        }
        if (this.disease == 'Diabetes') {
            return 'D';
        }
        if (this.disease == 'Tuberculosis') {
            return 'T';
            ;
        }
        if (this.disease == 'Dead') {
            return 'X';
        }
    }
    setDisease(newdisease) {
        this.disease = newdisease;
    }
}
exports.Patient = Patient;
Patient.NOT_IMPLEMENTED_MESSAGE = 'Disease keyword not exist';
//# sourceMappingURL=patients.js.map