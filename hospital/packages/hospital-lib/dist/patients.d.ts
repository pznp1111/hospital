import { Disease } from "./diseases";
export declare class Patient {
    private static readonly NOT_IMPLEMENTED_MESSAGE;
    private disease;
    private size;
    constructor(disease: string, size: number);
    getSize(): number;
    increment(qty: number): void;
    clear(): void;
    getDisease(): Disease;
    getDiseaseShortForm(): string;
    setDisease(newdisease: string): void;
}
