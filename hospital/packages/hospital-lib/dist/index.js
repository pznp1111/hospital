"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TemporaryQuarantine = exports.Quarantine = void 0;
var quarantine_1 = require("./quarantine");
Object.defineProperty(exports, "Quarantine", { enumerable: true, get: function () { return quarantine_1.Quarantine; } });
Object.defineProperty(exports, "TemporaryQuarantine", { enumerable: true, get: function () { return quarantine_1.TemporaryQuarantine; } });
//# sourceMappingURL=index.js.map