export declare enum Medicine {
    Aspirin = "As",
    Antibiotic = "An",
    Insulin = "I",
    Paracetamol = "P"
}
export declare var medicineList: Medicine[];
export declare var medicineShortFormList: string[];
export declare function medicationShortFormStringtoObj(disease: string): Medicine;
