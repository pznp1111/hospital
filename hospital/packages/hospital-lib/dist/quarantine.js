"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TemporaryQuarantine = exports.Quarantine = void 0;
const patients_1 = require("./patients");
const diseases_1 = require("./diseases");
const medications_1 = require("./medications");
class Quarantine {
    //type Product = { productId: number; price: number; discount: number };
    /***
     * Args:PatientsRegister with the following format `{F:NP,H:NP,D:NP,T:NP,X:NP}`
     *  F, H, D, T, X are patients’ health status codes;
        - F: Fever
        - H: Healthy
        - D: Diabetes
        - T: Tuberculosis
        - X: Dead
     *  NP is a number of patients for a given state;
     *  E.g. `{F:0,H:2,D:0,T:0,X:1}` means there are two healthy patients and one that is dead.
     *
     * ** */
    constructor(patients) {
        this.patientList = [];
        this.medicationHistory = [];
        this.patients = patients;
        for (const [diseasestr, count] of Object.entries(patients)) {
            console.log("diseasestr", diseasestr);
            if (diseases_1.diseaseShortFormList.includes(diseasestr)) {
                console.log("diseasestr1", diseasestr);
                var dis = ((0, diseases_1.DiseaseMapShortFormToFull)(diseasestr));
                this.patientList.push(new patients_1.Patient(dis, count));
            }
        }
    }
    /***  define which drugs will be given to all patients.
     * The argument to this method is an array of drugs codes,
        - As: Aspirin
        - An: Antibiotic
        - I: Insulin
        - P: Paracetamol
     * e.g. [“As”,“I”] means patients will be treated with Aspirin and Insulin;
     *
     */
    setDrugs(drugs) {
        drugs.forEach((medicine) => {
            if (medications_1.medicineShortFormList.includes(medicine)) {
                // Medicine medication = this.medicineMap[medicine];
                this.medicationHistory.push((0, medications_1.medicationShortFormStringtoObj)(medicine));
            }
        });
        console.log("this.medicationHistory", this.medicationHistory);
        // for (const [disease, count] of Object.entries(this.patients)) {
        //     console.log(disease, count);
        //     if(this.cureMap.hasOwnProperty(disease)){
        //     }
        //   } 
    }
    /** will administer the drugs to the patients;
     *
     */
    wait40Days() {
        console.log("this.medicationHistory", this.medicationHistory);
        console.log("this.patientlist", this.patientList);
        this.patientList.forEach((patient, index) => {
            console.log("patient status", (0, diseases_1.DiseaseMapToFull)(patient.getDisease()));
            const newDisease = this.drugEffect(patient);
            console.log("patient new  status:::", (0, diseases_1.DiseaseMapToFull)(newDisease));
            //patient.setDisease(DiseaseMapToFull(newDisease));
            if (newDisease != patient.getDisease()) {
                var size = patient.getSize();
                patient.increment(-1 * patient.getSize());
                //this.setPatientNumberFromListByDisease(patient.getDisease(),-1*patient.getSize());
                this.setPatientNumberFromListByDisease(newDisease, size);
            }
        });
        console.log("report2********************", this.patients);
        console.log("this.patientlist", this.patientList);
        this.makePatients();
        console.log("report3********************", this.patients);
    }
    /*** should return an object describing the current states of the patients, following the same format as the constructor argument.
     *
     */
    report() {
        console.log("report1********************", this.patients);
        return this.patients;
        // return this.patients;
    }
    makePatients() {
        // this.patientList.forEach((patient) => {
        //     // console.log(`${key}: ${mobile[key]}`);
        //     //console.log("report0********************",this.patients[patient.getDiseaseShortForm()]);
        //     this.patients[patient.getDiseaseShortForm()] = patient.getSize();
        //     //Object.assign(target, source)
        // });
        for (var i = 0; i < this.patientList.length; i++) {
            if (this.patientList[i].getDiseaseShortForm() !== undefined) {
                this.patients[this.patientList[i].getDiseaseShortForm()] = this.patientList[i].getSize();
            }
        }
    }
    setPatientNumberFromListByDisease(disease, increment) {
        for (var i = 0; i < this.patientList.length; i++) {
            if (this.patientList[i].getDisease() == disease) {
                this.patientList[i].increment(increment);
                //this.patients[this.patientList[i].getDiseaseShortForm()] = this.patientList[i].getSize();
            }
        }
    }
    drugEffect(patient) {
        //prevent death first
        if ((this.medicationHistory.includes(medications_1.Medicine.Paracetamol) && this.medicationHistory.includes(medications_1.Medicine.Aspirin))
            || (patient.getDisease() == diseases_1.Disease.Diabetes && !this.medicationHistory.includes(medications_1.Medicine.Insulin))) {
            console.log("loop1");
            return diseases_1.Disease.Dead;
        }
        //1. Aspiring and paracetamol cures fever  2. antibiotic cures tuberculosis
        if (patient.getDisease() == diseases_1.Disease.Fever && this.medicationHistory.some(medicine => [medications_1.Medicine.Aspirin, medications_1.Medicine.Paracetamol].includes(medicine))
            || patient.getDisease() == diseases_1.Disease.Tuberculosis && this.medicationHistory.includes(medications_1.Medicine.Antibiotic)) {
            console.log("loop2");
            return diseases_1.Disease.Healthy;
        }
        //insulin is mixed with antibiotic, healthy people catch Fever
        if (patient.getDisease() == diseases_1.Disease.Healthy && this.medicationHistory.includes(medications_1.Medicine.Insulin)
            && this.medicationHistory.includes(medications_1.Medicine.Antibiotic)) {
            console.log("loop3");
            return diseases_1.Disease.Fever;
        }
        console.log("loop4");
        return patient.getDisease();
    }
    DiseaseMapFullToShortForm(disease) {
        if (disease == diseases_1.Disease.Fever) {
            return 'F';
        }
        if (disease == diseases_1.Disease.Healthy) {
            return 'H';
        }
        if (disease == diseases_1.Disease.Diabetes) {
            return 'D';
        }
        if (disease == diseases_1.Disease.Tuberculosis) {
            return 'T';
        }
        if (disease == diseases_1.Disease.Dead) {
            return 'X';
        }
        return;
    }
}
exports.Quarantine = Quarantine;
// export class Quarantine {
//     private static readonly NOT_IMPLEMENTED_MESSAGE = 'Work, work.';
//     constructor(patients: PatientsRegister) {
//         throw new Error(Quarantine.NOT_IMPLEMENTED_MESSAGE);
//     }
//     public setDrugs(drugs: Array<string>): void {
//         throw new Error(Quarantine.NOT_IMPLEMENTED_MESSAGE);
//     }
//     public wait40Days(): void {
//         throw new Error(Quarantine.NOT_IMPLEMENTED_MESSAGE);
//     }
//     public report(): PatientsRegister {
//         throw new Error(Quarantine.NOT_IMPLEMENTED_MESSAGE);
//     }
// }
class TemporaryQuarantine {
    constructor(patients) {
        this.patients = patients;
    }
    setDrugs(drugs) { }
    wait40Days() { }
    report() {
        return this.patients;
    }
}
exports.TemporaryQuarantine = TemporaryQuarantine;
//# sourceMappingURL=quarantine.js.map