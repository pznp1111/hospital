export declare enum Disease {
    Fever = "F",
    Healthy = "H",
    Diabetes = "D",
    Tuberculosis = "T",
    Dead = "X"
}
export declare var diseaseShortFormList: string[];
export declare function DiseaseMapToFull(disease: Disease): string;
export declare function DiseaseMapShortFormToFull(disease: string): string;
