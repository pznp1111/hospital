export interface PatientsRegister {
    [key: string]: number;
}
export interface DiseaseMap {
    [key: string]: string;
}
export interface MedicineMap {
    [key: string]: string;
}
