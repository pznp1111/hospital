import {PatientsRegister,DiseaseMap,MedicineMap} from './patientsRegister';
import { Patient } from './patients';
import { Disease,DiseaseMapToFull,diseaseShortFormList,DiseaseMapShortFormToFull } from './diseases';
import { Medicine,medicineList,medicationShortFormStringtoObj, medicineShortFormList } from './medications';

export class Quarantine {

    private patients: PatientsRegister;
    public patientList:Array<Patient> = [];
    private medicationHistory:Array<Medicine> = [];

    //type Product = { productId: number; price: number; discount: number };
    /***
     * Args:PatientsRegister with the following format `{F:NP,H:NP,D:NP,T:NP,X:NP}`
     *  F, H, D, T, X are patients’ health status codes;
        - F: Fever
        - H: Healthy
        - D: Diabetes
        - T: Tuberculosis
        - X: Dead
     *  NP is a number of patients for a given state;
     *  E.g. `{F:0,H:2,D:0,T:0,X:1}` means there are two healthy patients and one that is dead.
     * 
     * ** */
    constructor(patients: PatientsRegister) {
        this.patients = patients;
        for (const [diseasestr, count] of Object.entries(patients)) {
            console.log("diseasestr",diseasestr)
            if (diseaseShortFormList.includes(diseasestr)){
                console.log("diseasestr1",diseasestr)

                var dis = (DiseaseMapShortFormToFull(diseasestr))
                this.patientList.push(new Patient(dis,count));
            }
        }
    }
    
    /***  define which drugs will be given to all patients.
     * The argument to this method is an array of drugs codes, 
        - As: Aspirin
        - An: Antibiotic
        - I: Insulin
        - P: Paracetamol
     * e.g. [“As”,“I”] means patients will be treated with Aspirin and Insulin;
     * 
     */
    public setDrugs(drugs: Array<string>): void {


            drugs.forEach((medicine) => {
                if (medicineShortFormList.includes(medicine)){
                    // Medicine medication = this.medicineMap[medicine];
                    this.medicationHistory.push(medicationShortFormStringtoObj(medicine));
                } 
            });

            //console.log("this.medicationHistory",this.medicationHistory)


        // for (const [disease, count] of Object.entries(this.patients)) {
        //     console.log(disease, count);
        //     if(this.cureMap.hasOwnProperty(disease)){

        //     }
        //   } 
    }

    /** will administer the drugs to the patients;
     * 
     */
    public wait40Days(): void {
        console.log("this.medicationHistory",this.medicationHistory)
        console.log("this.patientlist",this.patientList)
        this.patientList.forEach((patient, index) => {
            console.log("patient status",DiseaseMapToFull(patient.getDisease()));
            const newDisease = this.drugEffect(patient);
            console.log("patient new  status:::",DiseaseMapToFull(newDisease));
            //patient.setDisease(DiseaseMapToFull(newDisease));
            if(newDisease != patient.getDisease()){
                var size = patient.getSize()
                patient.increment(-1*patient.getSize());
                //this.setPatientNumberFromListByDisease(patient.getDisease(),-1*patient.getSize());
                this.setPatientNumberFromListByDisease(newDisease,size);
            }
            
        })
        console.log("report2********************",this.patients);
        console.log("this.patientlist",this.patientList)
        this.makePatients();
        console.log("report3********************",this.patients);

    }

    /*** should return an object describing the current states of the patients, following the same format as the constructor argument.
     * 
     */
    public report(): PatientsRegister {
        
        console.log("report1********************",this.patients);
        return this.patients;
        // return this.patients;
    }

    private makePatients():void{
        // this.patientList.forEach((patient) => {
        //     // console.log(`${key}: ${mobile[key]}`);
        //     //console.log("report0********************",this.patients[patient.getDiseaseShortForm()]);
        //     this.patients[patient.getDiseaseShortForm()] = patient.getSize();

        //     //Object.assign(target, source)
        // });

        for(var i =0;i<this.patientList.length;i++){
            if (this.patientList[i].getDiseaseShortForm()!==undefined){
                this.patients[this.patientList[i].getDiseaseShortForm()] = this.patientList[i].getSize();
            }
            
        }
    }

    private setPatientNumberFromListByDisease(disease:Disease,increment:number){
        for(var i =0;i<this.patientList.length;i++){
            if (this.patientList[i].getDisease()==disease){
                this.patientList[i].increment(increment);
                //this.patients[this.patientList[i].getDiseaseShortForm()] = this.patientList[i].getSize();
            }
            
        }
    }


    private drugEffect(patient:Patient):Disease{
        //prevent death first
        if ((this.medicationHistory.includes(Medicine.Paracetamol)  && this.medicationHistory.includes(Medicine.Aspirin))
        || (patient.getDisease() == Disease.Diabetes && !this.medicationHistory.includes(Medicine.Insulin))
        ){
            console.log("loop1");
            return Disease.Dead
        }
        //1. Aspiring and paracetamol cures fever  2. antibiotic cures tuberculosis
        if (patient.getDisease() == Disease.Fever && this.medicationHistory.some(medicine =>[Medicine.Aspirin,Medicine.Paracetamol].includes(medicine))
        || patient.getDisease() == Disease.Tuberculosis && this.medicationHistory.includes(Medicine.Antibiotic)
        ){
            console.log("loop2");
            return Disease.Healthy
        }
        //insulin is mixed with antibiotic, healthy people catch Fever
        if(patient.getDisease() == Disease.Healthy && this.medicationHistory.includes(Medicine.Insulin) 
        && this.medicationHistory.includes(Medicine.Antibiotic)){
            console.log("loop3");
            return Disease.Fever
        }
        console.log("loop4");
        return patient.getDisease();
        
    }

    

    // private DiseaseMapFullToShortForm(disease:Disease):string{
    //     if(disease ==  Disease.Fever){
    //         return 'F';
    //     }
    //     if(disease == Disease.Healthy){
    //         return 'H';
    //     }
    //     if(disease == Disease.Diabetes){
    //         return 'D';
    //     }
    //     if(disease == Disease.Tuberculosis){
    //         return 'T';
    //     }
    //     if(disease == Disease.Dead){
    //         return 'X';
    //     }
    //     return 
    // }


}


// export class Quarantine {

//     private static readonly NOT_IMPLEMENTED_MESSAGE = 'Work, work.';

//     constructor(patients: PatientsRegister) {
//         throw new Error(Quarantine.NOT_IMPLEMENTED_MESSAGE);
//     }

//     public setDrugs(drugs: Array<string>): void {
//         throw new Error(Quarantine.NOT_IMPLEMENTED_MESSAGE);
//     }

//     public wait40Days(): void {
//         throw new Error(Quarantine.NOT_IMPLEMENTED_MESSAGE);
//     }

//     public report(): PatientsRegister {
//         throw new Error(Quarantine.NOT_IMPLEMENTED_MESSAGE);
//     }
// }



export class TemporaryQuarantine {

    private patients: PatientsRegister;

    constructor(patients: PatientsRegister) {
        this.patients = patients;
    }
    
    public setDrugs(drugs: Array<string>): void {}

    public wait40Days(): void {}

    public report(): PatientsRegister {
        return this.patients;
    }
}
