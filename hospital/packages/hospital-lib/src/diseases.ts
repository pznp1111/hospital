
export enum Disease {
    Fever = 'F',
    Healthy = 'H',
    Diabetes = 'D',
    Tuberculosis = 'T',
    Dead = 'X',
}

export var diseaseShortFormList =["F","H","D","T","X"];


export function DiseaseMapToFull(disease:Disease):string{
    if(disease ==  Disease.Fever){
        return 'Fever';
    }
    if(disease == Disease.Healthy){
        return 'Healthy';
    }
    if(disease == Disease.Diabetes){
        return 'Diabetes';
    }
    if(disease == Disease.Tuberculosis){
        return 'Tuberculosis';
    }
    if(disease == Disease.Dead){
        return 'Dead';
    }
    return 
}

export function DiseaseMapShortFormToFull(disease:string):string{
    if(disease == 'F'){
        return 'Fever';
    }
    if(disease == 'H'){
        return 'Healthy';
    }
    if(disease == 'D'){
        return 'Diabetes';
    }
    if(disease == 'T'){
        return 'Tuberculosis';
    }
    if(disease == 'X'){
        return 'Dead';
    }
    return 
}

// - F: Fever
// - H: Healthy
// - D: Diabetes
// - T: Tuberculosis
// - X: Dead


