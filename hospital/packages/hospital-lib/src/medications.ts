export enum  Medicine {

    Aspirin = 'As',
    Antibiotic = 'An',
    Insulin = 'I',
    Paracetamol = 'P',

}

export var medicineList = [Medicine.Aspirin,Medicine.Antibiotic,Medicine.Insulin,Medicine.Paracetamol]
export var medicineShortFormList = ['As','An','I',"P"]
export function medicationShortFormStringtoObj(disease:string):Medicine{
    if(disease == 'As'){
        return Medicine.Aspirin;
    }
    if(disease == 'An'){
        return Medicine.Antibiotic;
    }
    if(disease == 'I'){
        return Medicine.Insulin;
    }
    if(disease == 'P'){
        return Medicine.Paracetamol;
    }

    return 
}