import { Disease } from "./diseases";
import { Medicine } from "./medications";
//import {MedicineMap} from './patientsRegister';
export class Patient {
    


    private static readonly NOT_IMPLEMENTED_MESSAGE = 'Disease keyword not exist';
    private disease: string;
    private size:number;
    //private medicationHistory:Array<Medicine>;
    // private readonly diseaseMap ={
    //     "F": "Fever",
    //     "H": "Healthy",
    //     "D": "Diabetes",
    //     "T": "Tuberculosis",
    //     "X": "Dead"
    //   }
    constructor(disease: string, size:number) {
        this.disease = disease;
        this.size = size;

        // if (!this.diseaseMap.hasOwnProperty(disease)){
        //     throw new Error(Patient.NOT_IMPLEMENTED_MESSAGE);
        // } else{
        //     this.disease = disease;
        // }   
    }

    public getSize():number{
        return this.size;
    }

    public  increment(qty:number):void {
        this.size+=qty;
    }

    public  clear():void {
        this.size = 0;
    }

    public getDisease ():Disease {

        if(this.disease == 'Fever'){
            return Disease.Fever;
        }
        if(this.disease == 'Healthy'){
            return Disease.Healthy;
        }
        if(this.disease == 'Diabetes'){
            return Disease.Diabetes;
        }
        if(this.disease == 'Tuberculosis'){
            return Disease.Tuberculosis;
        }
        if(this.disease == 'Dead'){
            return Disease.Dead;
        }
           
        //return this.disease;
    }

    public getDiseaseShortForm ():string {

        if(this.disease == 'Fever'){
            return 'F';
        }
        if(this.disease == 'Healthy'){
            return 'H';
        }
        if(this.disease == 'Diabetes'){
            return 'D';
        }
        if(this.disease == 'Tuberculosis'){
            return 'T';;
        }
        if(this.disease == 'Dead'){
            return 'X';
        }
    }

    public setDisease(newdisease:string):void{
        this.disease = newdisease;
    }

    // public setDrug(drug: Medicine): void {
    //     // Medicine m = new Medicine();
    //     this.medicationHistory.push(drug);
    // }

    // public setDisease(disease: string): void {
    //     if (!this.diseaseMap.hasOwnProperty(disease)){
    //         throw new Error(Patient.NOT_IMPLEMENTED_MESSAGE);
    //     } else{
    //         this.disease = disease;
    //     }   
        
    // }

    // public getDrug():Array<string>{
    //     return this.medicationHistory;
    // }
    // public getDisease():string{
    //     return this.disease;
    // }
}