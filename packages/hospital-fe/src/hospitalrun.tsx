import { PatientsRegister } from 'hospital-lib';
import CSS from 'csstype';
interface ServiceResponse<T> {
    success: boolean;
    error?: string;
    data?: T
  }
const tableStyle: CSS.Properties = {
    fontFamily: 'sans-serif',
    fontSize: '1.0rem'
  };


export const helpers = {

    fetchSimulationData()
    {
        return Promise.all([
            this.restApi("/patients").then<ServiceResponse<PatientsRegister>>(response => {
              var { success, error, data = "" } = response;
              //console.log("/patients",response);
              if (success) {
                return {
                  ...response,
                  data: data
                    .split(",")
                    .reduce<PatientsRegister>((o, s) => {
                      o[s] += 1;
                      return o;
                    }, this.emptyRegister())
                }
              } else {
                return { success, error }
              }
            }),
            this.restApi("/drugs").then<ServiceResponse<string[]>>(response => {
              var { success, error, data = "" } = response;
              if (success) {
                return {
                  ...response,
                  data: data.split(",")
                }
              }
              return { success, error }
            })
          ]).then(([patientsReponse, drugsReponse]) => {
            if (patientsReponse.error || drugsReponse.error) {
              throw [patientsReponse.error, drugsReponse.error]
            }
      
            return {
              drugs: drugsReponse.data,
              patientRegister: patientsReponse.data
            }
          }).catch(errors => alert(JSON.stringify(errors)))
    },


    createNewRow(modifingRowIndex:number,drugdisplay:string,rowValue:any,classes:object){
        var tempRow = this.createData(
            modifingRowIndex,drugdisplay,
            "",
            (rowValue['F']).toString(),
            (rowValue['H']).toString(),
            (rowValue['D']).toString(),
            (rowValue['T']).toString(),
            (rowValue['X']).toString(),
            classes
          );
          return tempRow;
    },

    createSimulateRow(modifingRowIndex:number,drugdisplay:string,prevRowValue:any,currentRowValue:any,classes:object){
        var tempRow = this.createData(
            modifingRowIndex,drugdisplay,
            "",
            (prevRowValue['fever']).toString() +"->" + (currentRowValue['F']).toString(),
            (prevRowValue['healthy']).toString() +"->" +(currentRowValue['H']).toString(),
            (prevRowValue['diabetic']).toString() +"->" +(currentRowValue['D']).toString(),
            (prevRowValue['tuberculosis']).toString() +"->" +(currentRowValue['T']).toString(),
            (prevRowValue['dead']).toString() +"->" +(currentRowValue['X']).toString(),
            classes
          );
          return tempRow;
    },
  
    createData(id:number,drugs:string, result:string, fever:string, healthy:string, diabetic:string, tuberculosis:string, dead:string,classes:object) {
      return { id,drugs, result, fever, healthy, diabetic, tuberculosis, dead, classes};
    },
  
    createClass(class1:CSS.Properties,class2:CSS.Properties,class3:CSS.Properties,class4:CSS.Properties,class5:CSS.Properties){
      return {'one':class1,'two':class2,'three':class3,'four':class4,'five':class5}
    },
  
    emptyRegister(){
      return { F: 0, H: 0, D: 0, T: 0, X: 0 }
    },

    
  
    mapDrup(drug:string){
  //   "": "None",
  //   As: "Aspirin",
  //   An: "Antibiotic",
  //   I: "Insulin",
  //   P: "Paracetamol",
  //   "An,I": "Antibiotic + Insulin",
  //   "As,P": "Paracetamol + Aspirin"
      switch (drug) {
        case 'As':
            return 'Aspirin';
        case 'An':
          return 'Antibiotic';
        case 'I':
          return 'Insulin';
        case 'P':
          return 'Paracetamol';
        case 'An,I':
          return 'Antibiotic + Insulin';
        case 'As,P':
          return 'Paracetamol + Aspirin';
        case '':
            return 'None';
        default:
          return '';
      }
    },
    
  
    restApi (path: string){
      return fetch("http://localhost:7200" + path).then<ServiceResponse<string>>(resp => {
        if (resp.status === 200) {
          return resp.json().then(data => ({
            success: true,
            data
          }))
        } else {
          return {
            success: false,
            error: `Something happened. Backend sends us status: ${resp.status}`
          }
        }
      })
  
    }  ,
  }