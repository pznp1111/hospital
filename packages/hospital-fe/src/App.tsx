import React from 'react';
//import ReactDOM from "react-dom";
//import { makeStyles } from '@material-ui/core';
import Button from '@mui/material/Button';
// import helpers from './helpers';
import './App.css';
import { PatientsRegister } from 'hospital-lib';
import {  Quarantine } from 'hospital-lib';
import { styled } from '@mui/material/styles';
import TablePagination from '@mui/material/TablePagination';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
//import { makeStyles } from "@material-ui/core/styles";
import CSS from 'csstype';
import {helpers} from './hospitalrun';
//import { stepClasses } from '@mui/material';




const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));


const redColor: CSS.Properties = {
    color: 'red',
};

const blueColor: CSS.Properties = {
    color: 'blue',
};

const blackColor: CSS.Properties = {
  color: 'black',
};

interface Props {
  //classes:styles
}

interface State {
  runningKey:number;
  reports:any[];
  rows:any[];
  resultRows:any[];
  rowsDisplay:any[];
  quarantineList:Quarantine[];
  drugList:any[];
  page:number;
  rowsPerPage:number;
}

export default class App extends React.Component<Props, State> {

  static defaultProps = {  };
  static propTypes = {
  };

  constructor(props: any) {
    super(props);
    this.state = {
      runningKey:0,  //for running index to be created in frontend
      reports:[],
      rows:[],  //to store raw table 
      resultRows:[], //to store result data, number of items could not be more than rows
      rowsDisplay:[], //to store maximum 10 rows (1 page) to be displayed in frontend, it is a subset of rows
      quarantineList:[], //to store Quarentine objects of each row
      drugList:[], //to store raw data, drug list of each row
      page:0, //current page index
      rowsPerPage:10 //total number of rows can be displayed per page
    };
  }

  componentDidMount() {
    this.autoSaveInterval();
  }



  /***
   * to handle page display of (10 rows maximum)
   */
  handlePageDisplay(newpage:number){
    var starting = newpage * this.state.rowsPerPage;
    var ending = starting + this.state.rowsPerPage;
    if(this.state.rows.length<=ending){
      ending = this.state.rows.length
    }
    var temp = this.state.rows.slice(starting, ending);
    this.setState({rowsDisplay:temp}, () => {
    });

  }



  autoSaveInterval(){
    setInterval(() => {
      this.newSimulate();
      this.runSumulation();
    }, 30000);
  }

  /***fetch new patient and drug data  */
  newSimulate = () =>{
    return helpers.fetchSimulationData()
    .then((data) => {


      var { drugs, patientRegister } = data as any;
      return { drugs, patientRegister }
    }).then(({ drugs, patientRegister }) => {
      this.createSimulation(patientRegister, drugs);
    })
  }

  /**run simulation based on the new patient data already fetched */
  runSumulation = () =>{
    var nextRow = this.state.resultRows.length;
    if(this.state.quarantineList.length-1 >=nextRow && this.state.drugList.length-1 >=nextRow) {
      var drugs = this.state.drugList[nextRow];
      this.state.quarantineList[nextRow].setDrugs(drugs);
      this.state.quarantineList[nextRow].wait40Days()
    } else{
      //alert("insertion error");
      return;
    }

    this.simulateRow(
      { key: nextRow,
       drugs,
       result: this.state.quarantineList[nextRow].report(),
      }
    );
  }


  /***handle simulation of a row */
  simulateRow = (report: any) =>{
    var reportsTmp = [report, ...this.state.reports];
    var modifingRowIndex = report['key'];
    var prevRowValue = this.state.rows[modifingRowIndex];
    var classes = this.makeClass(prevRowValue,reportsTmp[0]['result']);
    var drugdisplay = "";
    reportsTmp[0]['drugs'].forEach(function(item:string){
       drugdisplay += helpers.mapDrup(item)+","
    })
    if(drugdisplay.length != 0){
      drugdisplay = drugdisplay.substring(0, drugdisplay.length - 1);
    }
    var tempRow = helpers.createSimulateRow( modifingRowIndex,drugdisplay,prevRowValue,reportsTmp[0]['result'],classes);
    this.state.rows[modifingRowIndex] = tempRow;
    this.state.resultRows.push(tempRow);
    this.setState({resultRows:this.state.resultRows})
    this.setState({rows:this.state.rows}, () => {
      this.handlePageDisplay(this.state.page);
    });


  }

   /*** to create color difference if disease get better or worse for each cell */
  makeClass = (prevRowValue:any,currentRowValue:any)=>{
    var class1 = blackColor;
    var class2 = blackColor;
    var class3 = blackColor;
    var class4 = blackColor;
    var class5 = blackColor;

    if(prevRowValue['fever'] > currentRowValue['F']){
      class1 = blueColor;
    }
    if(prevRowValue['fever'] < currentRowValue['F']){
      class1 = redColor;
    }
    if(prevRowValue['healthy'] > currentRowValue['H']){
      class2 = blueColor;
    }
    if(prevRowValue['healthy'] < currentRowValue['H']){
      class2 = redColor;
    }
    if(prevRowValue['diabetic'] > currentRowValue['D']){
      class3 = blueColor;
    }
    if(prevRowValue['diabetic'] < currentRowValue['D']){
      class3 = redColor;
    }
    if(prevRowValue['tuberculosis'] > currentRowValue['T']){
      class4 = blueColor;
    }
    if(prevRowValue['tuberculosis'] < currentRowValue['T']){
      class4 = redColor;
    }
    if(prevRowValue['dead'] > currentRowValue['X']){
      class5 = blueColor;
    }
    if(prevRowValue['dead'] < currentRowValue['X']){
      class5 = redColor;
    }
    return helpers.createClass(class1,class2,class3,class4,class5);


  }


  /***create simuation based on a row  */
  createSimulation = (patientRegister: PatientsRegister, drugs: string[]) => {

    
    var simulator = new Quarantine(patientRegister);
    simulator.setDrugs(drugs);
    this.setState({runningKey:this.state.runningKey+1})
    this.makeNewRow(
      { key: this.state.runningKey,
       drugs,
       result: simulator.report(),
       //prior: { ...patientRegister }
      }
    );

    this.state.quarantineList.push(simulator);
    this.setState({quarantineList:this.state.quarantineList})
    this.state.drugList.push(drugs)
    this.setState({drugList:this.state.drugList});
   

  }

  /***display new data  */
  makeNewRow = (report: any) =>{
    var reportsTmp = [report, ...this.state.reports];
    // console.log("reportsTmp",reportsTmp);
    var drugdisplay = "";
    reportsTmp[0]['drugs'].forEach(function(item:string){
       drugdisplay += helpers.mapDrup(item)+","
    })
    if(drugdisplay.length !=0){
      drugdisplay = drugdisplay.substring(0, drugdisplay.length - 1);
    }
    var classes = helpers.createClass(blackColor,blackColor,blackColor,blackColor,blackColor);
    var tempRow = helpers.createNewRow(this.state.runningKey,drugdisplay,reportsTmp[0]['result'],classes);
    
    this.state.rows.push(tempRow);
    this.setState({rows:this.state.rows}, () => {
      this.handlePageDisplay(this.state.page);
    });


  }


  handleChangePage = (event: unknown, newPage:number) => {    
    this.handlePageDisplay(newPage);
    this.setState({page:newPage})
  };


render(){
  return (
    <div className="App" style={{ width: "100%", height: "100% " }}>

      <Box sx={{ flexGrow: 1 }}>
      <Grid container >
        <Grid item xs={6}>
        
          <Item>  <Button variant="contained" onClick={this.newSimulate}>New Simulation</Button></Item>
        </Grid>
        <Grid item xs={6}>
          <Item><Button  variant="contained" onClick={this.runSumulation}>Run Simulation</Button></Item>
        </Grid>
      </Grid>


      <Grid container >
        <Paper sx={{ width: '100%', mb: 2 }}>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell width={20}>Id</TableCell>
                  <TableCell align="right">Administered Drugs</TableCell>
                  <TableCell align="right">Fever</TableCell>
                  <TableCell align="right">Healthy</TableCell>
                  <TableCell align="right">Diabetic</TableCell>
                  <TableCell align="right">Tuberculosis</TableCell>
                  <TableCell align="right">Dead</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.rowsDisplay.map((row,index) => (
                  <TableRow
                    key={row.id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row" width={20}>
                      {row.id}
                    </TableCell>
                    <TableCell align="right"><div >{row.drugs}</div></TableCell>

                    <TableCell align="right"><div style={row.classes.one}>{row.fever}</div></TableCell>
                    <TableCell align="right"><div style={row.classes.two}>{row.healthy}</div></TableCell>
                    <TableCell align="right"><div style={row.classes.three}>{row.diabetic}</div></TableCell>
                    <TableCell align="right"><div style={row.classes.four}>{row.tuberculosis}</div></TableCell>
                    <TableCell align="right"><div style={row.classes.five}>{row.dead}</div></TableCell>
                  </TableRow>

                ))}
              </TableBody>
            </Table>
          </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10]}
              component="div"
              count={this.state.rows.length}
              rowsPerPage={10}
              page={this.state.page}
              onPageChange={this.handleChangePage}
            />
        </Paper>
      </Grid>


      </Box>
    </div>

  );
}
};





