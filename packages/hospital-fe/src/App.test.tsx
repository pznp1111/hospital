import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from "@testing-library/user-event";
import App from "./App"


test('renders screen', () => {
  render(<App />);
  console.log("screen",screen)
  const linkElement = screen.getByText(/Administered Drugs/i);
  
  expect(linkElement).toBeInTheDocument();
});


test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/Administered Drugs/i);
  
  expect(linkElement).toBeInTheDocument();
});


