import {PatientsRegister} from './patientsRegister';
import { Patient } from './patients';
import { Disease,diseaseShortFormList,DiseaseMapShortFormToFull } from './diseases';
import { Medicine,medicationShortFormStringtoObj, medicineShortFormList } from './medications';

export class Quarantine {

    private patients: PatientsRegister;
    public patientList:Array<Patient> = [];
    private medicationHistory:Array<Medicine> = [];

    /***
     * Args:PatientsRegister with the following format `{F:NP,H:NP,D:NP,T:NP,X:NP}`
     *  F, H, D, T, X are patients’ health status codes;
        - F: Fever
        - H: Healthy
        - D: Diabetes
        - T: Tuberculosis
        - X: Dead
     *  NP is a number of patients for a given state;
     *  E.g. `{F:0,H:2,D:0,T:0,X:1}` means there are two healthy patients and one that is dead.
     * 
     * ** */
    constructor(patients: PatientsRegister) {
        this.patients = patients;
        for (const [diseasestr, count] of Object.entries(patients)) {
           // console.log("diseasestr",diseasestr)
            if (diseaseShortFormList.includes(diseasestr)){
                //console.log("diseasestr1",diseasestr)

                var dis = (DiseaseMapShortFormToFull(diseasestr))
                this.patientList.push(new Patient(dis,count));
            }
        }
    }
    
    /***  define which drugs will be given to all patients.
     * The argument to this method is an array of drugs codes, 
        - As: Aspirin
        - An: Antibiotic
        - I: Insulin
        - P: Paracetamol
     * e.g. [“As”,“I”] means patients will be treated with Aspirin and Insulin;
     * 
     */
    public setDrugs(drugs: Array<string>): void {

            drugs.forEach((medicine) => {
                if (medicineShortFormList.includes(medicine)){
                    this.medicationHistory.push(medicationShortFormStringtoObj(medicine));
                } 
            });
    }

    /** will administer the drugs to the patients;
     * 
     */
    public wait40Days(): void {
        this.patientList.forEach((patient) => {
            const newDisease = this.drugEffect(patient);
            if(newDisease != patient.getDisease()){
                var size = patient.getSize()
                patient.increment(-1*patient.getSize());
                 this.setPatientNumberFromListByDisease(newDisease,size);
            }
            
        })
        this.makePatients();
    }

    /*** should return an object describing the current states of the patients, following the same format as the constructor argument.
     * 
     */
    public report(): PatientsRegister {
        return this.patients;
    }

    /****
     * 
     *  to update patients status from Patients list variable  
     */
    private makePatients():void{
        for(var i =0;i<this.patientList.length;i++){
            if (this.patientList[i].getDiseaseShortForm()!==undefined){
                this.patients[this.patientList[i].getDiseaseShortForm()] = this.patientList[i].getSize();
            }          
        }
    }

    /***
     * helper function update PatientList item size based on Disease
     * input: disease(Disease),increment(number)
     * 
     */
    private setPatientNumberFromListByDisease(disease:Disease,increment:number):void{
        for(var i =0;i<this.patientList.length;i++){
            if (this.patientList[i].getDisease()==disease){
                this.patientList[i].increment(increment);
             }
            
        }
    }

    /*** helper function to update Patient's disease based on the drups given to  them
     *  input: patient(Patient)
     * 
    */
    private drugEffect(patient:Patient):Disease{
        //prevent death first
        if ((this.medicationHistory.includes(Medicine.Paracetamol)  && this.medicationHistory.includes(Medicine.Aspirin))
        || (patient.getDisease() == Disease.Diabetes && !this.medicationHistory.includes(Medicine.Insulin))
        ){
            return Disease.Dead
        }
        //1. Aspiring and paracetamol cures fever  2. antibiotic cures tuberculosis
        if (patient.getDisease() == Disease.Fever && this.medicationHistory.some(medicine =>[Medicine.Aspirin,Medicine.Paracetamol].includes(medicine))
        || patient.getDisease() == Disease.Tuberculosis && this.medicationHistory.includes(Medicine.Antibiotic)
        ){
            return Disease.Healthy
        }
        //insulin is mixed with antibiotic, healthy people catch Fever
        if(patient.getDisease() == Disease.Healthy && this.medicationHistory.includes(Medicine.Insulin) 
        && this.medicationHistory.includes(Medicine.Antibiotic)){
            return Disease.Fever
        }
        return patient.getDisease();
        
    }

}





export class TemporaryQuarantine {

    private patients: PatientsRegister;

    constructor(patients: PatientsRegister) {
        this.patients = patients;
    }
    
    public setDrugs(drugs: Array<string>): void {}

    public wait40Days(): void {}

    public report(): PatientsRegister {
        return this.patients;
    }
}
