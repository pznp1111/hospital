import { Disease,DiseaseMapFullToObject,DiseaseMapFulltoShortForm} from "./diseases";
export class Patient {
    


    //private static readonly NOT_IMPLEMENTED_MESSAGE = 'Disease keyword not exist';
    private disease: string;
    private size:number;
    constructor(disease: string, size:number) {
        this.disease = disease;
        this.size = size;
    }

    public getSize():number{
        return this.size;
    }

    public increment(qty:number):void {
        this.size+=qty;
    }

    public clear():void {
        this.size = 0;
    }


    public getDisease ():Disease {
        return DiseaseMapFullToObject(this.disease);
    }

    public getDiseaseShortForm ():string {
        return DiseaseMapFulltoShortForm(this.disease);
    }

    public setDisease(newdisease:string):void{
        this.disease = newdisease;
    }

}