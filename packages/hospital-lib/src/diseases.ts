
export enum Disease {
    Fever = 'F',
    Healthy = 'H',
    Diabetes = 'D',
    Tuberculosis = 'T',
    Dead = 'X',
}

export var diseaseShortFormList =["F","H","D","T","X"];


export function DiseaseMapToFull(disease:Disease):string{
    if(disease ==  Disease.Fever){
        return 'Fever';
    }
    if(disease == Disease.Healthy){
        return 'Healthy';
    }
    if(disease == Disease.Diabetes){
        return 'Diabetes';
    }
    if(disease == Disease.Tuberculosis){
        return 'Tuberculosis';
    }
    if(disease == Disease.Dead){
        return 'Dead';
    }
    return 
}

export function DiseaseMapFulltoShortForm(disease:string):string{
    if(disease == 'Fever'){
        return 'F';
    }
    if(disease == 'Healthy'){
        return 'H';
    }
    if(disease == 'Diabetes'){
        return 'D';
    }
    if(disease == 'Tuberculosis'){
        return 'T';;
    }
    if(disease == 'Dead'){
        return 'X';
    }
}

export function DiseaseMapShortFormToFull(disease:string):string{
    if(disease == 'F'){
        return 'Fever';
    }
    if(disease == 'H'){
        return 'Healthy';
    }
    if(disease == 'D'){
        return 'Diabetes';
    }
    if(disease == 'T'){
        return 'Tuberculosis';
    }
    if(disease == 'X'){
        return 'Dead';
    }
    return 
}

export function DiseaseMapFullToObject(disease:string):Disease{
    if(disease == 'Fever'){
        return Disease.Fever;
    }
    if(disease == 'Healthy'){
        return Disease.Healthy;
    }
    if(disease == 'Diabetes'){
        return Disease.Diabetes;
    }
    if(disease == 'Tuberculosis'){
        return Disease.Tuberculosis;
    }
    if(disease == 'Dead'){
        return Disease.Dead;
    }
}

// - F: Fever
// - H: Healthy
// - D: Diabetes
// - T: Tuberculosis
// - X: Dead


