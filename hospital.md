# Hospital coding challenge

We would like to ask you to do a fun coding exercise to have a deeper understanding of your engineering and development skills. Imagine that your code will be read and modified by other developers, so they should be able to understand it easily. You should also remember that code should be easily extensible, maintainable, and scalable but at the same time beware of overly complicated solutions.

Last but not least: there is no single best solution, so we encourage you to design and implement the solution in a way you think is right.

This exercise will be composed of two parts:
  1. Creation of a small library implementing the core logic of the problem;
  2. Refactoring of the UI that uses this library

Project Structure
- root folder
  - packages
    - hospital-be
    - hospital-fe
    - hospital-lib

Each package is
  - *hospital-lib* will contain the implementation of the library (Part 1 of this document),
  - *hospital-fe* Is a not so good implementation of the UI which you will have to improve in any way you want (Part 2 of this document),
  - *hospital-be* is a small nodejs backend that we provide (its usage is explained below - it acts as the hospital’s server).

To run the application (you must have `yarn` installed on your machine)
- in the root directory execute `yarn install`
- in `hospital-be` directory 
  - `yarn start`
- in `hospital-lib` directory
  - `yarn run build`
- in `hospital-fe` directory
  - `yarn start`

## Problem description

You were asked by a doctor friend to prepare for her a “Hospital simulator”, which can simulate the future patients’ state, based on their current state and a list of drugs they take.

Patients can have one of these states:
  - F: Fever
  - H: Healthy
  - D: Diabetes
  - T: Tuberculosis
  - X: Dead

In the “Hospital simulator”, drugs are provided to all patients. It is not possible to target a specific patient. This is the list of available drugs:
  - As: Aspirin
  - An: Antibiotic
  - I: Insulin
  - P: Paracetamol

Drugs can change patients’ states. They can cure, cause side effects or even kill a patient if not properly prescribed.

Drugs effects are described by the following rules:
  - Aspirin cures Fever;
  - Antibiotic cures Tuberculosis;
  - A sick patient not receiving the right medicines remains sick, if not explicitly mentioned otherwise;
  - Insulin prevents diabetic subject from dying, does not cure Diabetes;
  - If insulin is mixed with antibiotic, healthy people catch Fever;
  - Paracetamol cures Fever;
  - Paracetamol kills subject if mixed with aspirin.

While applying the rules, please keep in mind that:
  - During one simulation a patient can change his state only once (only one rule can be applied).
  - Rules causing Death take precedence over others.

## Part 1. Creating the library *(hospital-lib)*

The goal is here to create a library implementing the specifications mentioned above. We provide you with a basic test file (*quarantine.spec.ts*) for the main test cases. Feel free to enrich it and create other tests for your internal code.

**Important.** Make sure that your solution is easily scalable in term of new patient states and new drugs. Also, make sure that your code is well tested.

### Implementation

You need to complete the *Quarantine* class that must provide:
  - A **constructor**.
  
    The argument of the constructor is an object describing the states of the patients, following the format: `{F:NP,H:NP,D:NP,T:NP,X:NP}`. Where:
    - F, H, D, T, X are patients’ health status codes;
    - NP is a number of patients for a given state;

    E.g. `{F:0,H:2,D:0,T:0,X:1}` means there are two healthy patients and one that is dead.
    
  - A method **setDrugs** that should define which drugs will be given to all patients.

    The argument to this method is an array of drugs codes, e.g. [“As”,“I”] means patients will be treated with Aspirin and Insulin;

  - A method **wait40Days** that will administer the drugs to the patients;

  - A method **report** that should return an object describing the current states of the patients, following the same format as the constructor argument.

## Technical information

A basic skeleton in TypeScript is provided in the *hospital-lib* folder. You **must** use it, otherwise your test will not be considered.

To run the tests, run the following commands in the *hospital-lib* folder:
 - `yarn test` - to run the basic predefined tests

## Part 2. Improving the UI *(hospital-fe)*

The doctor is now happy with the lib that has the core logic you implemented. But the UI that was developed by an amateur from a freelance website was just a prototype and didn't do a proper implementation. It is not maintainable and not scalable at all. 

The UI fetches the list of patients and list of drugs from a small server (*hospital-be*). It then runs the simulation using the data from `hospital-be` and uses `TemporaryQuarantine` from `hospital-lib`. Please change it so that it will use the `Qurantine` class you have implemented.
(see line 4 of `App.tsx`) 

### As you have already seen the UI contains
  - A button "Simulate" that pulls data from the `hospital-be` and immediately runs the simulation then shows the simulation result on the screen.

### What you need to improve
  - UI
    - Split the "Simulate" button's functionality by replacing it with 2 buttons, namely: "New Simulation" and "Run Simulation",
    - "New Simulation" - will fetch data from `hospital-be` and show this data on the screen (patients & drugs),
    - "Run Simulation" - will run the simulation using the `Quarantine` class from `hospital-lib`
  - Maintainability
    - All things are in one file right now. And it is quite a mess. Please improve it to make it easy to maintain and easy to add new features in the future
    - Feel free to improve typings (Typescript)
  - Functionality
    - Please limit the number of simulation results on the screen to only 10.

### Good to have (if time permits)
1. An auto-simulate feature that runs the simulation every 30 seconds
2. Manual entry - allow the user to enter patients and drugs combination and run the simulation using these inputs

### Feel free to
- Improve the look and feel (if time permits or if you feel like it)
- Use any library that you want to add

**Important.** Your solution must be simple yet clear and usable (we value user-friendliness). We highly value the quality of the implementation in terms of simplicity, maintainability, and stability (improve the UI as much as you can so as to reduce potential bugs in the future)

## Technical information

The UI is currently implemented using the library/framework you used in your recent projects (React). 

Putting all things together
- in the root directory execute `yarn install`
- in `hospital-be` directory 
  - `yarn start`
- in `hospital-lib` directory
  - `yarn run build`
- in `hospital-fe` directory
  - `yarn start`

## Using the Hospital server *(hospital-be)*

The server is accessible through the url http://localhost:7200. It provides two endpoints (the formats follow the ones given in the test file of the library implemented in part 1):

  - `GET /patients`
    
    Returns a string containing the list of current patients in the hospital. E.g. "D,F,F”.
  - `GET /drugs`
    
    Returns a string containing the list of drugs to administer to the patients. E.g. “As,I”.
